from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from . import models
from .forms import TestForm


def index(request):
    form = TestForm(request.POST or None)
    context = {
            'form': form
    }
    if form.is_valid():
        username = form.cleaned_data.get('username')
        age = form.cleaned_data.get('age')

        test = models.TestModel(name=username, age=age)
        test.save()

        context = {
            'form': form,
            'username': username,
            'age': age
        }

    return render(request, 'Home/index.html', context)


def TestModelRecords(request):
    data = models.TestModel.objects.all()
    context = {
            'testmodels': data
        }
    return render(request, 'Test/testmodels.html', context)


def EditTest(request, id):
    ErrorsList = []
    if request.POST:
        id = request.POST.get('id')
        age = request.POST.get('age')
        name = request.POST.get('name')
        testRecord = models.TestModel.objects.get(id=id)

        if not age:
            ErrorsList.append("Age is required")
        if name == '':
            ErrorsList.append("Name is required")

        if len(ErrorsList) > 0:
            context = {
                'testRecord': testRecord,
                'has_errors': True,
                'errors': ErrorsList
            }
            return render(request, 'Test/editTest.html', context)

        testRecord.age = age
        testRecord.name = name
        testRecord.save()
        return redirect('test')
    else:
        testRecord = models.TestModel.objects.get(id=id)
        context = {
                'testRecord': testRecord
            }
        return render(request, 'Test/editTest.html', context)


def DeleteTest(request, id):
    models.TestModel.objects.filter(id=id).delete()
    return redirect('test')

@csrf_exempt
def hello(request, ID):
    return HttpResponse("Hello World! " + str(ID))
