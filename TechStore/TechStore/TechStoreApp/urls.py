from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('hello/<int:ID>/', views.hello, name='hello'),
    path('test/', views.TestModelRecords, name='test'),
    path('test/delete/<int:id>/', views.DeleteTest, name='deleteTest'),
    path('test/edit/<int:id>/', views.EditTest, name='editTest'),
]
